<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Socialite;

use Illuminate\Routing\Controller;


class AuthController extends Controller
{
    //login github redirect
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    //handle on sucees log in
    public function handleProviderCallback()
    {
        $user = Socialite::driver('github')->user();
        //add user to database with Oauth id
        $exist=User::where('provider_id',$user->id)->first();
        if(User::where('provider_id',$user->id)->exists()){
            auth()->login($exist);
        }else{
            $authUser=new User();
            $authUser->provider_id=$user->id;
            $authUser->name=$user->username;
            $authUser->email=$user->email;
            $authUser->avatar=$user->avatar;
            $authUser->provider='github';
            $authUser->save();
            auth()->login($authUser);
        }

        return view('home');
    }
}
