<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GithubController extends Controller
{
    //create new repository
    public function index(){
        $user=Auth::user();
        return view('git.new_repo', compact('user'));
    }
    //edit repository
    public function edit($repo_name){
        return view('git.edit_repo',compact('repo_name'));
    }
    //get contents of a repo
    public function contents($repo_name){
        return view('git.content',compact('repo_name'));

    }
    //get commits of a repo
    public function commits($repo_name){
     return view('git.commits',compact('repo_name'));
    }
    //get branches of a repo
    public  function branches($repo_name){
     return view('git.branches',compact('repo_name'));
    }
    //get releases of a repo
    public function releases($repo_name){
        return view('git.releases',compact('repo_name'));
    }

}
