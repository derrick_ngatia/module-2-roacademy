@extends('layouts.app')
@section('content')
    <h3 class="text-center">{{$repo_name}}</h3>
    <edit username="{{Auth::user()->name}}" repo_name="{{$repo_name}}"></edit>
    @endsection