@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
          <div class="col-md-2">
              <img class="text-center" src="{{Auth::user()->avatar}}" height="90px" width="90px" style="border-radius: 50%"><br>
              <p>{{Auth::user()->name}}</p>
              <p>{{Auth::user()->email}}</p>
              <p class="text-center"><a href='{{url("/new_repo")}}' class="btn btn-primary">Add Repository</a><i class="fas fa-plus "> </i></p>
          </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                 <example-component username="{{Auth::user()->name}}"></example-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
