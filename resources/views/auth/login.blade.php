@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Login with your GITHUB account</div>

                <div class="panel-body text-center">
                    <a href='{{url("/oauth/github")}}'><i class="fab fa-github fa-10x text-center"></i></a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
