@extends('layouts.app')
@section('content')
    <h5 class="text-center">{{Auth::user()->name}}/{{$repo_name}}</h5>
    <contents username="{{Auth::user()->name}}" repo_name="{{$repo_name}}"></contents>
    @endsection