<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/oauth/github', 'AuthController@redirectToProvider');
Route::get('/new_repo','GithubController@index');
Route::get('/edit_repo/{repo_name}','GithubController@edit');
Route::get('/commits/{repo_name}','GithubController@commits');
Route::get('/branches/{repo_name}','GithubController@branches');
Route::get('/releases/{repo_name}','GithubController@releases');
Route::get('/contents/{repo_name}','GithubController@contents');
Route::get('/oauth/github/callback', 'AuthController@handleProviderCallback');