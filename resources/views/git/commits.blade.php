@extends('layouts.app')
@section('content')
    <h5 class="text-center">{{Auth::user()->name}}/{{$repo_name}}</h5>
    <div class="col-md-12">
        <commits username="{{Auth::user()->name}}" repo_name="{{$repo_name}}"></commits>
    </div>
@endsection