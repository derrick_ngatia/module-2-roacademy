
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
import Vue from 'vue'
import VueResource from 'vue-resource'
import Router from 'vue-router'
Vue.use(VueResource);
Vue.use(Router)
import GitHubAPI from 'vue-github-api'
Vue.use(GitHubAPI, { token: '09630c516d5c90c9d6b8c6d13263e13eeb4e6e0d' })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

Vue.component('example-component', require('./components/ExampleComponent.vue')); //list repositories
Vue.component('repo', require('./components/Repo.vue')); //contents of a repositoey
Vue.component('edit', require('./components/Edit.vue')); //edit a repositoey
Vue.component('contents', require('./components/Content.vue'));// expandable contents of repo
Vue.component('commits', require('./components/Commits.vue')); //commits of a repo
Vue.component('releases', require('./components/Releases.vue'));//releases of a repo
Vue.component('branches', require('./components/Branches.vue'));// branches of a repo






const app = new Vue({
    el: '#app'
});
