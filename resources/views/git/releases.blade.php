@extends('layouts.app')
@section('content')
    <h5 class="text-center">{{Auth::user()->name}}/{{$repo_name}}</h5>
    <div class="col-md-12">
        <releases username="{{Auth::user()->name}}" repo_name="{{$repo_name}}"></releases>
    </div>
@endsection