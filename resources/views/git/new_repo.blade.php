@extends('layouts.app')
@section('content')
    <div class="col-md-2">

    </div>
    <div class="panel panel-default col-md-9">
        <div class="panel-heading text-center">{{$user->name}} /<i class="far fa-plus-square fa-2x"> Create new Repository</i></div>
        <repo name="{{$user->name}}"></repo>
    </div>
    @endsection